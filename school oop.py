from abc import ABC, abstractmethod


class School:
    def __init__(self, name, number, location):
        self.name = name
        self.number = number
        self.location = location

    def check_info(self):
        return (print(f"{self.name}. number: {self.number}, located in {self.location}"))


class Room:
    def __init__(self, room_number, max_places, level):
        self.room_number = room_number
        self.max_places = max_places
        self.level = level

    @abstractmethod
    def room_name(self):
        pass

    def check_room_destiny(self):
        return(print(f"This room is {self.room_name()}"))

    def check_where_is(self):
        print(f"Room no. {self.room_number} is located at {self.level} floor")

    def check_max_places(self):
        print(f"In room no. {self.room_number} can accommodate a maximum of {self.max_places} people")


class TeachersRoom(Room):
    def room_name(self):
        return "Teachers Room"


class SportHall(Room):
    def room_name(self):
        return "Sports Hall"
    def game_create(self, competition, start):
        return(print(f"{competition} start at: {start}"))


class Toilets(Room):
    def room_name(self):
        return "Toilet"


class OrdinaryClassRoom(Room):
    def room_name(self):
        return "Ordinary Class"


class SpecialClassRoom(Room):
    def room_name(self):
        return "Class for advanced lesson"


class Person:
    def __init__(self, name, last_name, birth_date,):
        self.name = name
        self.last_name = last_name
        self.birth_date = birth_date

    @abstractmethod
    def role_in_school(self):
        pass

    def person_introduce(self):
        print(f"{self.role_in_school()}: {self.name} {self.last_name}, born in {self.birth_date}")


class Teacher(Person):
    def role_in_school(self):
        return "teacher"

    def __init__(self, name, last_name, birth_date, specialization):
        super().__init__(name, last_name, birth_date)
        self.specialization = specialization

    def check_specialization(self):
        print(f"Teacher {self.name} {self.last_name} specialization: {self.specialization}")


class Students(Person):
    def role_in_school(self):
        return "student"

    def __init__(self, name, last_name, birth_date, id_number):
        super().__init__(name, last_name, birth_date)
        self.id_number = id_number

    def check_id(self):
        print(f"Student {self.name} {self.last_name} ID number: {self.id_number}")


class Staff(Person):
    def role_in_school(self):
        return "staff"

    def __init__(self, name, last_name, birth_date):
        super().__init__(name, last_name, birth_date)
        self.tasks_list = []

    def add_task(self):
        new_task = input(f"Add new task for {self.name} {self.last_name}:  ")
        self.tasks_list.append(new_task)

    def show_tasks(self):
        print("--------------------------------------------------")
        print(f"Tasks for {self.name} {self.last_name}:")
        for task in self.tasks_list:
            print(f"* {task}")
        print("--------------------------------------------------")




szkola1 = School("Szkoła Podstawowa im. XYZ", 6, "Krakow")
szkola1.check_info()

hala1 = SportHall(1, 350, "parter")
hala1.check_room_destiny()
hala1.game_create("football game", "10:00 am")


klasa2 = OrdinaryClassRoom(2, 30, "parter")
klasa2.check_room_destiny()
klasa2.check_where_is()



john = Staff("John", "McQueen", "01.06.1977")
john.person_introduce()
john.add_task()
john.show_tasks()


barbra = Teacher("Barbra", "Blaze", "05.12.1989", "mathematician")
barbra.check_specialization()
barbra.person_introduce()


mike = Students("Mike", "Wezz", "01.10.2003", 3321)
mike.check_id()